package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"text/template"

	"gitlab.com/toolhub/toolhub/pkg/endpoint"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
)

var (
	configTemplate = template.Must(template.ParseFiles("templates/nginx.conf"))
)

func splitURL(hostname, location string) (string, string) {
	root := ""
	if len(location) == 0 {
		location = "/"
	}
	if i := strings.Index(location, "/"); i != -1 {
		root = location[:i]
		location = location[i:]
	} else {
		root = location
		location = "/"
	}
	if root == "" {
		root = strings.Trim(hostname, "*")
	}

	if !strings.HasSuffix(location, "/") {
		location = location + "/"
	}

	return root, location
}

type nginxLocation struct {
	Location string
	Sources  string
}

type nginxServer struct {
	ServerName string
	Locations  []nginxLocation
}

// HostTable is a structure that holds CSP directives for each CDS record.
type HostTable map[string]map[string]map[string]struct{} // hostname to map of locations and dispatchers.

// Add adds a new CSP directive to allow given CDS record to reach dispatcher at the specified location.
func (h HostTable) Add(hostname, location, dispatcher string) {
	serverName, directive := splitURL(hostname, location)

	dispatcherRoot, dispatcherPath := splitURL(hostname, dispatcher)
	dispatcherSource := ""
	if dispatcherRoot == "" {
		dispatcherSource = fmt.Sprintf("$http_host%s", dispatcherPath)
	} else {
		dispatcherSource = fmt.Sprintf("%s%s", dispatcherRoot, dispatcherPath)
	}

	if h[serverName] == nil {
		h[serverName] = map[string]map[string]struct{}{
			directive: {dispatcherSource: {}},
		}
	} else {
		h[serverName][directive][dispatcherSource] = struct{}{}
	}
}

func (h HostTable) build() []nginxServer {
	result := make([]nginxServer, 0, len(h))
	for hostname, directives := range h {
		locations := make([]nginxLocation, 0, len(directives))
		for location, sources := range directives {
			items := make([]string, 0, len(sources))
			for source := range sources {
				items = append(items, source)
			}
			locations = append(locations, nginxLocation{Location: location, Sources: strings.Join(items, " ")})
		}
		result = append(result, nginxServer{ServerName: hostname, Locations: locations})
	}
	return result
}

func saveConfig(path, nginx string) error {
	var config endpoint.Config

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	if err := json.NewDecoder(file).Decode(&config); err != nil {
		return fmt.Errorf("failed to JSON parse config: %s", err)
	}

	table := HostTable{}
	for hostname, endpoint := range config.Endpoints {
		table.Add(hostname, endpoint.Cds, endpoint.Dispatcher.Http)
	}

	nginxConfigFile, err := os.Create(nginx)
	if err != nil {
		return err
	}
	defer nginxConfigFile.Close()
	if configTemplate.Execute(nginxConfigFile, table.build()); err != nil {
		return err
	}

	return nil
}

func sendReloadSignal() error {
	cmd := exec.Command("nginx", "-s", "reload")
	cmd.Stderr = os.Stderr
	if err := cmd.Start(); err != nil {
		return err
	}
	return nil
}

func main() {
	var accessEndpointsPath, nginxConfig string

	flag.StringVar(&accessEndpointsPath, "endpoints-config", "", "path to the access endpoints json file.")
	flag.StringVar(&nginxConfig, "nginx-config", "/etc/nginx/nginx.conf", "path to the nginx config file.")

	flag.Parse()

	watcher := watcher.NewFileWatcher(accessEndpointsPath)
	if err := watcher.Start(); err != nil {
		log.Fatalf("failed to watch access endpoints file '%s': %s", accessEndpointsPath, err)
	}
	go func() {
		for {
			select {
			case <-watcher.Events:
				if err := saveConfig(accessEndpointsPath, nginxConfig); err != nil {
					log.Printf("failed to update nginx config: %s", err)
					continue
				}
				if err := sendReloadSignal(); err != nil {
					log.Printf("sending reload signal to nginx failed: %s", err)
				}
				log.Printf("reloaded nginx.")
			case err := <-watcher.Errors:
				fmt.Printf("watcher error: %s", err)
			}
		}
	}()

	if err := saveConfig(accessEndpointsPath, nginxConfig); err != nil {
		log.Printf("failed to create nginx config: %s", err)
	}

	// start the nginx
	cmd := exec.Command("nginx", "-c", nginxConfig, "-g", "daemon off;")
	log.Printf("%v", cmd)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdin

	signals := make(chan os.Signal, 5)
	signal.Notify(signals)
	go func() {
		for {
			select {
			case sig := <-signals:
				if proc := cmd.Process; proc != nil {
					if err := proc.Signal(sig); err != nil {
						log.Printf("failed to relay signal %v to process %d: %s", sig, proc.Pid, err)
					}
				}
			}
		}
	}()
	if err := cmd.Start(); err != nil {
		log.Fatalf("nginx failed: %s", err)
	}
	if err := cmd.Wait(); err != nil {
		log.Fatalf("nginx failed: %s", err)
	}
	log.Println("nginx unexpectedly exited.")
}
